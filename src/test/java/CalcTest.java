package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class CalcTest {

      public void testSlow() {
        System.out.println("slow");
      }

      public void testSlower() {
        System.out.println("slower");
      }

      public void testFast() {
        System.out.println("fast-updated");
      }

	@Test
      public void test1() {
        assertEquals("Result", 9, 9);
        MinMax object = new MinMax();
        object.functn(1,0);
        object.functn(1,1);
      }

      @Test
          public void test2() {
            assertEquals("Result", 9, 9);
            Duration object = new Duration();
            object.dur();
            object.calculateIntValue();
          }

          @Test
              public void test3() {
                assertEquals("Result", 9, 9);
                Usefulness object = new Usefulness();
                object.desc();
                object.functionWF();
              }

              @Test
                  public void test4() {
                    assertEquals("Result", 9, 9);
                    AboutCPDOF object = new AboutCPDOF();
                    object.desc();
                  }

                  @Test
                      public void test5() {
                        assertEquals("Result", 9, 9);
                        AboutCPDOF object = new AboutCPDOF();
                        object.desc();
                      }

    }
